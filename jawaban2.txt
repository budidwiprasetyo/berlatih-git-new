Soal 1 Mengambil Data dari Database

a. Mengambil data users kecuali password nya
SELECT name, email from users;

b. Mengambil data items
- Mendapatkan data item yang memiliki harga di atas 1.000.000 (satu juta)
SELECT * from items WHERE price > 1000000;

- Mengambil data item yang memiliki name serupa
SELECT * FROM `items` where name like "%watch";

c. Menampilkan data items join dengan kategori
SELECT items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name AS name from items LEFT JOIN categories on items.category_id = categories.id;

Soal 2 Mengubah Data dari Database
UPDATE `items` SET `price` = '2500000' WHERE `items`.`id` = 1;